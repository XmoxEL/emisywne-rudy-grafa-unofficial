## Społecznościowy fork "Emisywnych rud" grafa.
**Aby pobrać** rozwiń listę w **lewym górnym rogu** i wybierz wersję, która cię interesuje, a następnie **kliknij przycisk pobierania** znajdujący się obok przycisku `Clone` i wybierz `.zip`
Na chwilę obecną skupiam się na wersji CTM. Być może potem wezmę się za aktualizację do 1.17 animowanej wersji paczki.

Autor: **XmoxEL**
